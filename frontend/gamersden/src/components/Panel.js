import React, { Component } from 'react';
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import { AUTH_TOKEN } from '../constants';

const GET_CURRENT_USER = gql`
  query currentUser($token: String!) {
    currentUser(token: $token) {
      username
      isSuperuser
    }
  }
`;

class Panel extends Component{
    render(){
        const { data } = this.props
        if (data.loading || !data.currentUser) {
            return(
                <div>
                    <p>Nie jesteś zalogowany</p>
                </div>
            );
          }
        if (data.currentUser.isSuperuser){
        return(
          <div role="main" class="container">
            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
              <img class="mr-3" src="/docs/4.3/assets/brand/bootstrap-outline.svg" alt="" width="48" height="48"/>
              <div class="lh-100">
                <h6 class="mb-0 text-white lh-100">Bootstrap</h6>
                <small>Since 2011</small>
              </div>
            </div>
            <div class="my-3 p-3 bg-white rounded shadow-sm">
              <h6 class="border-bottom border-gray pb-2 mb-0">Recent updates</h6>
              <div class="media text-muted pt-3">
                <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-gray-dark">{data.currentUser.username}</strong>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  </p>
              </div>
              <div class="media text-muted pt-3">
                <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#e83e8c"/><text x="50%" y="50%" fill="#e83e8c" dy=".3em">32x32</text></svg>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">@username</strong>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  </p>
              </div>
              <div class="media text-muted pt-3">
                <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32"><title>Placeholder</title><rect width="100%" height="100%" fill="#6f42c1"/><text x="50%" y="50%" fill="#6f42c1" dy=".3em">32x32</text></svg>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">@username</strong>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  </p>
              </div>
              <small class="d-block text-right mt-3">
              <a href="#">All updates</a>
              </small>
            </div>
          </div>
        );
        }
        else if (!data.currentUser.isSuperuser){
            return(
                <div>
                    <p>Nie jesteś zalogowany jako administrator</p>
                </div>
            );
        }
    }
}

const queryOptions = {
    options: token => ({
      variables : {
        token: localStorage.getItem(AUTH_TOKEN),
      },
    }),
  }
  
Panel = graphql(GET_CURRENT_USER, queryOptions)(Panel)
export default Panel;