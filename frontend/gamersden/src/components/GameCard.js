import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class GameCard extends Component {
    render() {
        const media = "http://localhost:8000/gamersden/media/"
        const {id, name, rating, image, review } = this.props
        return (
            <div className="col-md-2">
            <div className="card mb-2">
            <Link to={`/game/${id}`} >
            <img className="card-img-top" src={media+image} alt={name} height="200px" width="100%"/>
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">{review.substr(0,40)+"..."}</p>
                <p className="card-text"><span class="badge badge-secondary">Ocena </span>{rating}</p>
            </div>
            </Link>
        </div>
        </div>
        )
    }
}

export default GameCard;