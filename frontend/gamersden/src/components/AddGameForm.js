import React, { Component } from 'react';
import { graphql, Mutation } from "react-apollo";
import gql from "graphql-tag";
import { AUTH_TOKEN } from '../constants';

const GET_CURRENT_USER = gql`
  query currentUser($token: String!) {
    currentUser(token: $token) {
      username
      isSuperuser
    }
  }
`;

const CREATE_RATING = gql`
  mutation createRating($username: String!, $rate: Int!, $review: String!, $game: String!){
    createRating(ratingData:{game: $game, user: {username: $username}, rate: $rate, comment: $review}){
      ok
    }
  }
`;

class AddGameForm extends Component{
    constructor(props) {
        super(props);
        this.state = {
            rate : 0,
            review : "",
        };
      }
    render(){
        const { data, game } = this.props
        const { rate, review } = this.state
        if (data.loading || !data.currentUser) {
            return(
                <div class="container">
                    <h1 class="mt-5">You have to be logged as user to comment</h1>
                </div>
            );
          }
        else{
          var username = data.currentUser.username
            return(
            <form>
                <div className="form-row align-items-center">
                    <label for="exampleFormControlSelect1">Example select</label>
                    <select className="custom-select mr-sm-2" id="exampleFormControlSelect1" onChange={e => this.setState({ rate: e.target.value })}>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    </select>
                </div>
                <div className="form-group">
                    <label for="exampleFormControlTextarea1">Example textarea</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" onChange={e => this.setState({ review: e.target.value })}>{data.currentUser.username}</textarea>
                </div>
                <div>
                  <Mutation 
                  mutation={CREATE_RATING}
                  variables={{ username, rate, review, game }}>
                  {mutation => (
                    <div className="pointer mr2 button" onClick={mutation}>
                      <input type="submit" value="Comment"/>
                    </div>
                    )}
                  </Mutation>
                </div>
            </form>
        )}
    }
}

const queryOptions = {
    options: token => ({
      variables : {
        token: localStorage.getItem(AUTH_TOKEN),
      },
    }),
  }
  
AddGameForm = graphql(GET_CURRENT_USER, queryOptions)(AddGameForm)
export default AddGameForm;