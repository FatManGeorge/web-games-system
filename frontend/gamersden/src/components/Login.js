import React, { Component } from 'react'
import { AUTH_TOKEN } from '../constants'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const CREATE_PROFILE = gql`
  mutation createProfile($password: String!, $username: String!) {
    createProfile(password: $password, username: $username) {
      ok
    }
    tokenAuth(username: $username, password: $password){
      token
  }
    
  }
`

const LOGIN_USER = gql`
    mutation loginUser($username: String!, $password: String!){
        loginUser(userData:{username: $username, password: $password}){
            ok
        }
        tokenAuth(username: $username, password: $password){
            token
        }
    }
`

class Login extends Component {
  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    username: '',
  }

  render() {
    const { login, password, username } = this.state
    return (
      <div className="container">
        <h4 className="mv3">{login ? 'Login' : 'Sign Up'}</h4>
        <div className="form-groupr">
            <input
              value={username}
              onChange={e => this.setState({ username: e.target.value })}
              type="text"
              placeholder="Your username"
            />
          </div>
          <div className="form-group">
          <span></span>
          <input
            value={password}
            onChange={e => this.setState({ password: e.target.value })}
            type="password"
            placeholder="Choose a safe password"
          />
        </div>
        <div className="form-group">
            <Mutation
                mutation={login ? LOGIN_USER : CREATE_PROFILE}
                variables={{ password, username }}
                onCompleted={data => this._confirm(data)}
            >
                {mutation => (
                <div className="pointer mr2 button" onClick={mutation}>
                    {login ? <input type="submit" value="Login"/> : <input type="submit" value="Sign In"/>}
                </div>
                )}
            </Mutation>
          <div
            className="pointer button"
            onClick={() => this.setState({ login: !login })}
          >
            {login
              ? 'need to create an account?'
              : 'already have an account?'}
          </div>
        </div>
      </div>
    )
  }

  _confirm = async data => {
    this._saveUserData(data.tokenAuth.token)
    this.props.history.push(`/`)
  }

  _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token)
  }
}

export default Login
