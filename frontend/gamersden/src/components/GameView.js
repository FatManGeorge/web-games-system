import React, { Component } from 'react';
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import ReactDOM from 'react-dom';

import AddGameForm from './AddGameForm';

const query = gql`
query GameView($id: Int!) {
  game(id: $id) {
    id
    name
    rating
    image
    review
    ratings{
        rate
        userComment
        user{
          user{
            username
          }
        }
      }
  }
}
`

class GameView extends Component {
  render() {
    let { data } = this.props
    if (data.loading || !data.game) {
      return <div>Loading...</div>
    }
    const name = data.game.name
    const media = "http://localhost:8000/gamersden/media/"
    return (
        ReactDOM.render({name}, document.getElementById('title')),
      <div className="container">
        <div className="jumbotron">
            <h1 className="display-2">{data.game.name}</h1>
            <img className="card-img-top col-2" src={media+data.game.image} alt={data.game.name} height="260px" width="100%"/><h1><span class="badge badge-secondary">Ocena </span>{data.game.rating}</h1>
            <p>{data.game.review}</p>
        </div>
        {data.game.ratings.map(({id, rate, userComment, user}) => (
        <div className="jumbotron">
          <h1><span class="badge badge-secondary">{rate}</span> {user.user.username}</h1>
          <p>{userComment}</p>
        </div>
        ))}
        <AddGameForm game={data.game.name}/>
      </div>
    )
  }
}

const queryOptions = {
  options: props => ({
    variables: {
      id: props.match.params.id,
    },
  }),
}

GameView = graphql(query, queryOptions)(GameView)
export default GameView