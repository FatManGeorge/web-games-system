import React, { Component } from 'react';
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import { AUTH_TOKEN } from '../constants';

const GET_USERS_RATINGS = gql`
  query currentUser($token: String!) {
    currentUser(token: $token) {
      username
      profile{
        ratingmodelSet{
          game{
            name
            image
          }
          rate
          userComment
        }
    }
  }
}
`;

class MyRatings extends Component{
    constructor(props) {
        super(props);
        this.state = {
            ratings : [],
            user : null,
            profile: null,
        };
      }
    render(){
      const { data } = this.props
      if (data.loading || !data.currentUser) {
        return(
            <div className="container">
                <h1 className="mt-5">You have to be logged to see your reviews</h1>
            </div>
        );
      }
      else{
        const user_rates = data.currentUser.profile.ratingmodelSet
        const media = "http://localhost:8000/gamersden/media/"
        if (user_rates.length === 0){
          return(
          <div className="container">
            <h1>No reviews given by you</h1>
          </div>
          )
        }
        else{
        return(
          <div className="container">
          {user_rates.map(({id, game, rate, userComment}) => (
            <div className="jumbotron">
              <img className="card-img-top col-2" alt={game.name} src={media+game.image} height="100%" width="100%"/>
              <h1><span class="badge badge-secondary">{rate}</span> {game.name}</h1>
              <p>{userComment}</p>
            </div>
            ))}
          </div>
        )
      }
    }
  }
}

const queryOptions = {
  options: token => ({
    variables : {
      token: localStorage.getItem(AUTH_TOKEN),
    },
  }),
}

MyRatings = graphql(GET_USERS_RATINGS, queryOptions)(MyRatings)
export default MyRatings;