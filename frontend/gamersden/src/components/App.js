import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import ApolloClient from 'apollo-client'
import { ApolloProvider } from "react-apollo";
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'



import './App.css';
import NavBar from './NavBar';
import { About } from './About';
import Login  from './Login';
import { Home } from './Home';
import GameView from './GameView';
import Panel from './Panel';
import MyRatings from './MyRatings';
import { AUTH_TOKEN } from '../constants'

const httpLink = createHttpLink({
  uri: 'http://localhost:8000/graphql'
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem(AUTH_TOKEN);
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div id='routes'>
          <NavBar />
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path="/game/:id" component={GameView}/>
            <Route path='/about' component={About}/>
            <Route path='/login' component={Login}/>
            <Route path='/panel' component={Panel}/>
            <Route path='/myratings' component={MyRatings}/>
          </Switch>
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
