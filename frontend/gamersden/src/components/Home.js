import React from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";

import GameCard from "./GameCard";

const GetGames = () => (
    <Query
      query={gql`
        {
            allGames {
              id
              name
              rating
              image
              review
          }
        }
      `}
    >
   {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return data.allGames.map(({id, name, rating, image, review, i }) => (
        <GameCard key={i}
                  id={id}
                  name={name}
                  rating={rating}
                  image={image}
                  review={review}
        />
      ));
    }}
    </Query>
  )
  

export const Home = () => {
    return(
        <div className="row jumbotron">
            <GetGames/>
            
        </div>
    );
}