import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MdAccountCircle, MdAccessibility } from "react-icons/md";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { AUTH_TOKEN } from '../constants'

import logo from '../components/favicon.ico';

const GetUser = () => {
    const token = localStorage.getItem(AUTH_TOKEN)
    if (token === '' || token === null)
    {
        return ('Guest');
    }
    else {
        return (<GetCurrentUser token={token}/>);
    }
};

const GET_CURRENT_USER = gql`
  query currentUser($token: String!) {
    currentUser(token: $token) {
      username
      isSuperuser
    }
  }
`;

const GetCurrentUser = ({ token }) => (
    <Query query={GET_CURRENT_USER} variables={{ token }}>
      {({ loading, data }) => {
        if (loading) return null;

        if (data.currentUser.isSuperuser){
            return (
            <text>
                {data.currentUser.username}
                <MdAccessibility size={28}/>
            </text>
            );
        }
        else if (data.currentUser.username){
            return (
                    data.currentUser.username
                );
    }}}
    </Query>
  );


class NavBar extends Component {
    render() 
    {
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <a href="/" className="navbar-brand" >
                    <Link to="/">
                        <img alt="Home" className="App-logo" src={logo} />
                    </Link>
                </a>
                <div className="collapse navbar-collapse" id="navbarsExample03">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link to="/">
                                <a className="nav-link">Home <span className="sr-only">(current)</span></a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/about">
                                <a className="nav-link">About</a>
                            </Link>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="https://example.com" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><MdAccountCircle size={28}/><GetUser/> </a>
                            <div className="dropdown-menu" aria-labelledby="dropdown03">
                                <Link to="/panel">
                                    <a className="dropdown-item">Admin Panel</a>
                                </Link>
                                <Link to="/myratings">
                                    <a className="dropdown-item">My ratings</a>
                                </Link>
                                <Link to="/login">
                                    <a className="dropdown-item">Login</a>
                                </Link>
                                <Link to="/">
                                    <a className="dropdown-item" onClick={this.logout}>Logout</a>
                                </Link>
                            </div>
                        </li>
                    </ul>
                    <form className="form-inline my-2 my-md-0">
                        <input className="form-control" type="text" placeholder="Search"/>
                    </form>
                </div>
            </nav>
                );
            }
    logout = () => {
        localStorage.removeItem(AUTH_TOKEN)
        }
}

export default NavBar;