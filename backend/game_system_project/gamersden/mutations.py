import graphene
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
import graphql_jwt

from gamersden.models import Category, OSModel, Profile, GameModel, RatingModel
from gamersden.schema import CategoryType, OSModelType, ProfileType, UserType, GameModelType, RatingModelType


class CreateCategory(graphene.Mutation):
    class Arguments:
        name = graphene.String()

    ok = graphene.Boolean()
    category = graphene.Field(CategoryType)

    @staticmethod
    def mutate(root, info, name):
        category = Category(name=name)
        category.save()
        ok = True
        return CreateCategory(category=category, ok=ok)


class CreateOSModel(graphene.Mutation):
    class Arguments:
        name = graphene.String()

    ok = graphene.Boolean()
    os_model = graphene.Field(OSModelType)

    @staticmethod
    def mutate(root, info, name):
        os_model = OSModel(name=name)
        os_model.save()
        ok = True
        return CreateOSModel(os_model=os_model, ok=ok)


class CreateProfile(graphene.Mutation):
    class Arguments:
        username = graphene.String()
        password = graphene.String()

    ok = graphene.Boolean()
    profile = graphene.Field(ProfileType)
    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, username, password):
        user = User.objects.create_user(username=username, password=password)
        user.save()
        profile = Profile(user=user)
        profile.save()
        ok = True
        return CreateProfile(profile=profile, user=user, ok=ok)


class UserInput(graphene.InputObjectType):
    username = graphene.String(required=True)
    new_username = graphene.String(required=False)
    password = graphene.String(required=False)


class UpdateProfile(graphene.Mutation):
    class Arguments:
        profile_data = UserInput(required=True)

    user = graphene.Field(UserType)
    profile = graphene.Field(ProfileType)

    @staticmethod
    def mutate(root, info, profile_data=None):
        if profile_data.new_username:
            user = User.objects.get(username=profile_data.username)
            user.username = profile_data.new_username
            user.save()
            profile = Profile.objects.get(user=user)
        return UpdateProfile(user=user, profile=profile)


class Upload(graphene.Scalar):
    def serialize(self):
        pass


class GameModelInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    os_supported = graphene.String()
    categories = graphene.String()
    image = Upload()
    review = graphene.String()


class CreateGameModel(graphene.Mutation):
    class Arguments:
        game_data = GameModelInput(required=True)

    game = graphene.Field(GameModelType)
    ok = graphene.Boolean()

    @staticmethod
    def mutate(root, info, game_data=None):
        print(info.context.FILES)
        game_data.image = info.context.FILES
        game = GameModel(name=game_data.name, review=game_data.review, image=game_data.image)
        game.save()
        categories = game_data.categories.split(',')
        oses = game_data.os_supported.split(',')
        for category in categories:
            for cat_model in Category.objects.all():
                if category == cat_model.name:
                    game.categories.add(cat_model)

        for os in oses:
            for os_model in OSModel.objects.all():
                if os == os_model.name:
                    game.operating_system.add(os_model)
        game.save()
        ok = True

        return CreateGameModel(game=game, ok=ok)


class RatingInput(graphene.InputObjectType):
    game = graphene.String(required=True)
    user = graphene.InputField(UserInput)
    rate = graphene.Int(required=True)
    comment = graphene.String()


class CreateRating(graphene.Mutation):
    class Arguments:
        rating_data = RatingInput(required=True)

    game = graphene.Field(GameModelType)
    user = graphene.Field(UserType)
    profile = graphene.Field(ProfileType)
    rate = graphene.Field(RatingModelType)
    ok = graphene.Boolean()

    @staticmethod
    def mutate(root, info, rating_data=None):
        game = GameModel.objects.get(name=rating_data.game)
        user = User.objects.get(username=rating_data.user.username)

        profile = Profile.objects.get(user=user)
        rating = RatingModel(user=profile, game=game, rate=rating_data.rate, user_comment=rating_data.comment)
        rating.save()
        ok = True
        return CreateRating(game=game, user=user, profile=profile, rate=rating, ok=ok)


class LoginUser(graphene.Mutation):
    class Arguments:
        user_data = UserInput(required=True)

    user = graphene.Field(UserType)
    ok = graphene.Boolean()

    @staticmethod
    def mutate(root, info, user_data=None):
        if user_data is not None:
            user = authenticate(username=user_data.username, password=user_data.password)
            sesja = info.context
            login(request=sesja, user=user, backend='django.contrib'
                                                    '.auth.backends '
                                                    '.ModelBackend')
            ok = True
        if user.is_authenticated:
            return LoginUser(user=info.context.user, ok=ok)
        else:
            ok = False
            return LoginUser(user=None, ok=ok)


class LogoutUser(graphene.Mutation):
    ok = graphene.Boolean()

    @staticmethod
    def mutate(root, info):
        if info.context.user.is_authenticated:
            logout(request=info.context)
            ok = True
        else:
            ok = False
        return LoginUser(ok=ok)


class Mutations(graphene.ObjectType):
    create_category = CreateCategory.Field()
    create_os_model = CreateOSModel.Field()
    create_profile = CreateProfile.Field()
    update_profile = UpdateProfile.Field()
    create_game = CreateGameModel.Field()
    create_rating = CreateRating.Field()
    login_user = LoginUser.Field()
    logout_user = LogoutUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
