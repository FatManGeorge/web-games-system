import graphene
from graphene_django.types import DjangoObjectType
from django.contrib.auth.models import User
from graphql_jwt.decorators import login_required

from gamersden.models import OSModel, Category, GameModel, Profile, RatingModel


class OSModelType(DjangoObjectType):
    class Meta:
        model = OSModel


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category


class GameModelType(DjangoObjectType):
    rating = graphene.String(source='rating')

    class Meta:
        model = GameModel


class UserType(DjangoObjectType):
    class Meta:
        model = User


class ProfileType(DjangoObjectType):
    class Meta:
        model = Profile


class RatingModelType(DjangoObjectType):
    class Meta:
        model = RatingModel


class Query(object):
    all_osmodels = graphene.List(OSModelType)
    osmodel = graphene.Field(OSModelType, id=graphene.Int(), name=graphene.String())

    all_category = graphene.List(CategoryType)
    category = graphene.Field(CategoryType, id=graphene.Int(), name=graphene.String())

    all_games = graphene.List(GameModelType)
    game = graphene.Field(GameModelType, id=graphene.Int(), name=graphene.String(),
                          pub_date=graphene.Date(), rating=graphene.Float())

    all_users = graphene.List(UserType)
    user = graphene.Field(UserType, id=graphene.Int(), username=graphene.String())
    current_user = graphene.Field(UserType, token=graphene.String(required=True))

    all_profiles = graphene.List(ProfileType)
    profile = graphene.Field(ProfileType, id=graphene.Int())

    all_ratingmodels = graphene.List(RatingModelType)
    rating = graphene.Field(RatingModelType, id=graphene.Int(), game=graphene.String(),
                            user_comment=graphene.String(), rate=graphene.Int())

    def resolve_all_osmodels(self, info, **kwargs):
        return OSModel.objects.all()

    def resolve_osmodel(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return OSModel.objects.get(pk=id)

        if name is not None:
            return OSModel.objects.get(name=name)

    def resolve_all_category(self, info, **kwargs):
        return Category.objects.all()

    def resolve_category(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return Category.objects.get(pk=id)

        if name is not None:
            return Category.objects.get(name=name)

    def resolve_all_games(self, info, **kwargs):
        return GameModel.objects.all()

    def resolve_game(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')
        pub_date = kwargs.get('pub_date')
        rating = kwargs.get('rating')

        if id is not None:
            return GameModel.objects.get(pk=id)
        if name is not None:
            return GameModel.objects.get(name=name)
        if pub_date is not None:
            return GameModel.objects.get(pub_date=pub_date)
        if rating is not None:
            return GameModel.objects.get(rating=rating)

    def resolve_all_users(self, info, **kwargs):
        return User.objects.all()

    def resolve_user(self, info, **kwargs):
        id = kwargs.get('id')
        username = kwargs.get('username')

        if id is not None:
            return User.objects.get(pk=id)
        if username is not None:
            return User.objects.get(username=username)

    @login_required
    def resolve_current_user(self, info, **kwargs):
        return info.context.user

    def resolve_all_profiles(self, info, **kwargs):
        return Profile.objects.all()

    def resolve_profile(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Profile.objects.get(pk=id)

    def resolve_all_ratingmodels(self, info, **kwargs):
        return RatingModel.objects.all()

    def resolve_rating(self, info, **kwargs):
        id = kwargs.get('id')
        game = kwargs.get('game')
        rate = kwargs.get('rate')

        if id is not None:
            return RatingModel.objects.get(pk=id)
        if game is not None:
            return RatingModel.game_model_set.get(name=game)
        if rate is not None:
            return RatingModel.objects.get(rate=rate)
