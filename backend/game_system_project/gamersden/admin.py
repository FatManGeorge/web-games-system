from django.contrib import admin

from gamersden.models import Category, OSModel, GameModel, Profile, RatingModel


# Register your models here.

class RatingModelInline(admin.TabularInline):
    model = RatingModel


class ProfileAdmin(admin.ModelAdmin):
    inlines = [
        RatingModelInline,
    ]


admin.site.register(Category)
admin.site.register(OSModel)
admin.site.register(GameModel)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(RatingModel)
