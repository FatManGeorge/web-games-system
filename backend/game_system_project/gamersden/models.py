from django.db import models
from django.utils.functional import cached_property
from django.contrib.auth.models import User


# Create your models here.


class OSModel(models.Model):
    name = models.TextField(max_length=30)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.TextField(max_length=30)

    def __str__(self):
        return self.name


class GameModel(models.Model):
    categories = models.ManyToManyField(Category, blank=True, related_name='categories')
    operating_system = models.ManyToManyField(OSModel, blank=True)
    name = models.TextField(max_length=50)
    review = models.TextField(max_length=10000)
    image = models.ImageField(blank=True, null=True)
    pub_date = models.DateField(auto_now_add=True)

    @cached_property
    def rating(self):
        score = 0.0
        for rate in self.ratings.all():
            score += rate.rate
        if score == 0.0:
            return 0
        score = score/len(self.ratings.all())
        if score == 10:
            return 10
        return round(score, 1)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    games_played = models.ManyToManyField(GameModel, blank=True)
    favorite_game_category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        return self.user.username


class RatingModel(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)
    game = models.ForeignKey(GameModel, on_delete=models.DO_NOTHING, related_name='ratings')
    rate = models.IntegerField(default=5)
    user_comment = models.TextField(max_length=200)

    def __str__(self):
        return str(self.user) + " " + str(self.game) + " " + str(self.rate)
