# Generated by Django 2.1.2 on 2018-11-14 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamersden', '0006_auto_20181114_2137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamemodel',
            name='categories',
            field=models.ManyToManyField(blank=True, related_name='categories', to='gamersden.Category'),
        ),
        migrations.AlterField(
            model_name='gamemodel',
            name='operating_system',
            field=models.ManyToManyField(blank=True, to='gamersden.OSModel'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='games_played',
            field=models.ManyToManyField(blank=True, to='gamersden.GameModel'),
        ),
    ]
