from django.test import TestCase
from gamersden.models import Category, OSModel, GameModel, RatingModel, Profile
from django.contrib.auth.models import User


class CategoryTestCase(TestCase):
    def setUp(self):
        Category.objects.create(name="RTS")
        Category.objects.create(name="RPG")

    def test_category_name(self):
        """Testing proper name representation"""

        self.assertEqual(Category.objects.all()[0].name, 'RTS')
        self.assertEqual(Category.objects.all()[1].name, 'RPG')


class OSModelTestCase(TestCase):
    def setUp(self):
        OSModel.objects.create(name="Linux")
        OSModel.objects.create(name="Android")

    def test_os_model_name(self):
        self.assertEqual(OSModel.objects.all()[0].name, 'Linux')
        self.assertEqual(OSModel.objects.all()[1].name, 'Android')


class GameModelTestCase(TestCase):
    def setUp(self):
        Category.objects.create(name="RTS")
        OSModel.objects.create(name="Linux")
        gameModelSet = {
            'name': 'Dota 2',
            'categories': Category.objects.get(name='RTS'),
            'operating_system': OSModel.objects.get(name='Linux')
        }
        GameModel.objects.create(**gameModelSet)

    def test_game_model_creation(self):
        self.assertEqual(GameModel.objects.all()[0].name, 'Dota 2')



class ProfileTestCase(TestCase):
    def setUp(self):
        Profile.objects.create(user=User.objects.create_user('TestUser'))

    def test_profile_mode(self):
        self.assertEqual(Profile.objects.all()[0].user.username,  'TestUser')
