import graphene

from gamersden.schema import Query
from gamersden.mutations import Mutations


class Query(Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutations)
